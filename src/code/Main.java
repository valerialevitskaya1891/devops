package code;

import code.ConsoleCalculator;

// (с) Copyright В.Р. Левицкая, А.А. Ломатов, 2023
public class Main {
    public static void main(String[] args) {
        ConsoleCalculator.main(args);
    }
}
