package test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import code.ConsoleCalculator;

public class UnitTest {

    @Test
    public void testAddition() {
        String input = "2 + 3";
        double expected = 5.0;

        double result = evaluateExpression(input);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testSubtraction() {
        String input = "5 - 3";
        double expected = 2.0;

        double result = evaluateExpression(input);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testMultiplication() {
        String input = "2 * 4";
        double expected = 8.0;

        double result = evaluateExpression(input);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testDivision() {
        String input = "10 / 5";
        double expected = 2.0;

        double result = evaluateExpression(input);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testDivisionByZero() {
        String input = "8 / 0";

        Assertions.assertThrows(ArithmeticException.class, () -> {
            evaluateExpression(input);
        });
    }

    @Test
    public void testInvalidInput() {
        String input = "2 + 3 *";

        Assertions.assertThrows(NumberFormatException.class, () -> {
            evaluateExpression(input);
        });
    }

    @Test
    public void testUnsupportedOperation() {
        String input = "2 ^ 3";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            evaluateExpression(input);
        });
    }

    private double evaluateExpression(String input) {
        ConsoleCalculator calculator = new ConsoleCalculator();
        return calculator.evaluateExpression(input);
    }
}
