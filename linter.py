import os

COPYRIGHT_STRING = "// (с) Copyright В.Р. Левицкая, А.А. Ломатов, 2023"


def check_copyright(filename):
    with open(filename, 'r') as file:
        first_line = file.readline()
        if first_line.strip() != COPYRIGHT_STRING:
            print(f"Missing or incorrect copyright header in file: {filename}")


def lint_project():
    for root, _, files in os.walk('.'):
        for file in files:
            if file.endswith('.java'):
                filepath = os.path.join(root, file)
                check_copyright(filepath)


lint_project()
